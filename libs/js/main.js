$(document).ready(function(){
  new WOW().init();

 $(document).scroll(function(){
            var y = $(this).scrollTop();
            if (y > 600) {
                $('.up').fadeIn(1000);
            } else {
                $('.up').fadeOut(1000);
                   }
        });
               
        $(".up").click(function(){
            $("html, body").animate({ scrollTop: 0},500);
            return false;
        });

  $('.carousel-slick').slick({
      dots: false,
      infinite: true,
      autoplay: true,
      speed: 300,
      slidesToShow: 2,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: false
          }
        },
        // You can unslick at a given breakpoint now by adding:
        // settings: "unslick"
        // instead of a settings object
      ]
      });

  $('.services-carousel-slick').slick({
      dots: false,
      infinite: true,
      autoplay: true,
      speed: 300,
      responsive: [
        {
          breakpoint: 2000,
          settings: "unslick" // destroys slick
        },
        {
          breakpoint: 992,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 2,
            infinite: true,
            arrows: false,
            dots: true
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            infinite: true,
            arrows: false
          }
        }
      ]
      });
    });